module totp

go 1.22.1

require (
	bitbucket.org/jbester/binaryio v0.0.0-20180908164458-e3e978037272
	github.com/howeyc/gopass v0.0.0-20170109162249-bf9dde6d0d2c
	github.com/pkg/errors v0.8.0
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)

require (
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc // indirect
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2 // indirect
	golang.org/x/crypto v0.0.0-20190103213133-ff983b9c42bc // indirect
	golang.org/x/sys v0.0.0-20190105165716-badf5585203e // indirect
)
