package main

import (
	"bufio"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"github.com/howeyc/gopass"
	"github.com/pkg/errors"
	"gopkg.in/alecthomas/kingpin.v2"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"strings"

	"totp"
)

var (
	verbose       = kingpin.Flag("verbose", "Verbose mode.").Short('v').Bool()
	generate      = kingpin.Command("generate", "Generate a totp token for an account")
	account       = generate.Arg("account", "Account name").String()
	add           = kingpin.Command("add", "Add a new totp account")
	newAccount    = add.Arg("account", "Account name").String()
	remove        = kingpin.Command("remove", "Remove a totp account")
	removeAccount = remove.Arg("account", "Accout name").String()
	list          = kingpin.Command("list", "List accounts")
	passphrase    = kingpin.Command("passphrase", "Set or remove a passphrase")
)

func Prompt(prompt string) (string, error) {
	fmt.Printf(prompt)
	var b = bufio.NewReader(os.Stdin)
	return b.ReadString('\n')
}

func DoGenerate(secret string) error {
	// if no secret passed in - ask for one
	if secret == "" {
		s, err := Prompt("Enter secret: ")
		if err != nil {
			return errors.Wrap(err, "cannot process input")
		}
		secret = s
	}

	// remove whitespace
	var trimmedSecret = strings.TrimSpace(secret)

	// create secret
	totpSecret, err := totp.Base32Secret(trimmedSecret)
	if err != nil {
		fmt.Println(err.Error())
		return errors.Wrap(err, "cannot create totp generator")
	}

	// create the generator
	var generator = totp.NewTotp(totpSecret)

	// generate the current token
	token, err := generator.Now()
	fmt.Printf("%06d\n", token)
	return err
}

func GetConfigDirectory() string {
	var homeDirectory string
	if runtime.GOOS == "windows" {
		homeDirectory = os.Getenv("APPDATA")
	} else {
		homeDirectory = os.Getenv("HOME")
	}
	return filepath.Join(homeDirectory, ".totpcmd")
}

func IsFolder(path string) bool {
	stat, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return stat.IsDir()
}

func Exists(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}
	return true
}

type Account struct {
	Name   string
	Secret string
}

func die(msg string) {
	fmt.Println(msg)
	os.Exit(1)
}

func GetConfigFilaName() string {
	return filepath.Join(GetConfigDirectory(), "accounts")
}

type EncryptionConfig struct {
	IV  []byte
	Key []byte
}

type Config struct {
	Accounts []Account
	*EncryptionConfig
}

func Encrypt(passwd []byte, iv []byte, plaintext []byte) []byte {
	if len(iv) != aes.BlockSize {
		panic("Internal error; iv isn't a block")
	}
	// encrypted
	var sha = sha256.New()
	sha.Write(iv)
	sha.Write(passwd)
	var hash = sha.Sum(nil)

	block, err := aes.NewCipher(hash[:32])
	if err != nil {
		panic(err)
	}
	var stream = cipher.NewCTR(block, iv)
	var encrypted = make([]byte, aes.BlockSize+len(plaintext))
	stream.XORKeyStream(encrypted[aes.BlockSize:], plaintext)
	copy(encrypted[:aes.BlockSize], iv)
	return encrypted
}

func Decrypt(passwd []byte, content []byte) []byte {
	// encrypted
	var iv = content[:aes.BlockSize]
	content = content[aes.BlockSize:]

	var sha = sha256.New()
	sha.Write(iv)
	sha.Write(passwd)
	var hash = sha.Sum(nil)

	block, err := aes.NewCipher(hash[:32])
	if err != nil {
		panic(err)
	}
	var stream = cipher.NewCTR(block, iv)
	var decrypted = make([]byte, len(content))
	stream.XORKeyStream(decrypted, content)
	return decrypted
}

const magicId = "totp0000"

func LoadConfig() (*Config, error) {
	var fp, err = os.Open(GetConfigFilaName())
	var config Config
	defer fp.Close()
	if err != nil {
		return nil, err
	}
	content, err := ioutil.ReadAll(fp)

	// test if encrypted
	if !reflect.DeepEqual(content[:len(magicId)], []byte(magicId)) {
		var done = false
		for !done {
			fmt.Printf("Enter passphrase: ")
			var b, err = gopass.GetPasswd()
			if err != nil {
				die(err.Error())
			}
			// attempt decryption
			var candidate = Decrypt(b, content)
			// test if it's a valid config
			if reflect.DeepEqual(candidate[:len(magicId)], []byte(magicId)) {
				config.EncryptionConfig = &EncryptionConfig{
					IV:  content[:aes.BlockSize],
					Key: b,
				}
				content = candidate
				done = true
			}
		}
	}
	if err != nil {
		return nil, err
	}
	// strip off the magic id
	content = content[len(magicId):]
	// unmarshall account data
	err = json.Unmarshal(content, &config.Accounts)
	if err != nil {
		return nil, err
	}

	return &config, nil
}

func SaveConfig(config *Config) error {
	var permissions os.FileMode = 0600

	st, err := os.Stat(GetConfigFilaName())
	if err != nil {
		return err
	}
	permissions = st.Mode()
	data, err := json.Marshal(config.Accounts)
	if err != nil {
		return err
	}

	data = append([]byte(magicId), data...)

	if config.EncryptionConfig != nil {
		data = Encrypt(config.Key, config.IV, data)
	}

	return ioutil.WriteFile(GetConfigFilaName(), data, permissions)
}

func GetNewPassword() ([]byte, error) {
	var done = false
	var password []byte
	var err error
	for !done {
		fmt.Printf("Enter the passphrase (empty for no passphrase):")
		password, err = gopass.GetPasswd()
		if err != nil {
			return nil, err
		}
		if string(password) != "" {
			fmt.Printf("Enter the same passphrase again:")
			password2, err := gopass.GetPasswd()
			if err != nil {
				return nil, err
			}
			if string(password) != string(password2) {
				fmt.Println("Passwords don't match")
			} else {
				done = true
			}
		} else {
			done = true
		}
	}
	return password, nil
}

func main() {
	var configFile *Config
	var err error
	var cmd = kingpin.Parse()

	if !Exists(GetConfigDirectory()) {
		err := os.MkdirAll(GetConfigDirectory(), 0700)
		if err != nil {
			die(err.Error())
		}
	}

	if Exists(GetConfigFilaName()) {
		configFile, err = LoadConfig()
		if err != nil {
			die(err.Error())
		}
	}

	switch cmd {
	case add.FullCommand():
		var accountName string
		if *newAccount == "" {
			name, err := Prompt("Account Name: ")
			if err != nil {
				die(err.Error())
			}
			accountName = strings.TrimSpace(name)
		} else {
			accountName = *newAccount
		}

		for _, account := range configFile.Accounts {
			if account.Name == accountName {
				die(fmt.Sprintf("Account named '%v' already exists", accountName))
			}
		}

		secret, err := Prompt("Secret: ")
		if err != nil {
			die(err.Error())
		}
		secret = strings.TrimSpace(secret)

		configFile.Accounts = append(configFile.Accounts, Account{Name: accountName, Secret: secret})
		if !Exists(GetConfigFilaName()) {
			fmt.Printf("Saving configuration to %v\n", GetConfigFilaName())
			var passphrase, err = GetNewPassword()
			if err != nil {
				die(err.Error())
			}
			if passphrase != nil {
				iv := make([]byte, aes.BlockSize)
				if _, err := io.ReadFull(rand.Reader, iv); err != nil {
					die(err.Error())
				}
				configFile.EncryptionConfig = &EncryptionConfig{
					Key: passphrase,
					IV:  iv,
				}
			}
		}

		SaveConfig(configFile)

	case remove.FullCommand():
		if configFile == nil {
			die("No config")
		}
		var accountName string
		if *removeAccount == "" {
			name, err := Prompt("Account Name : ")
			if err != nil {
				die(err.Error())
			}
			accountName = strings.TrimSpace(name)
		} else {
			accountName = *removeAccount
		}

		var found = false
		for i, account := range configFile.Accounts {
			if account.Name == accountName {
				configFile.Accounts = append(configFile.Accounts[0:i], configFile.Accounts[i+1:]...)
				found = true
				break
			}
		}

		if found {
			SaveConfig(configFile)
		}

	case generate.FullCommand():
		if *account == "" {
			if err := DoGenerate(""); err != nil {
				fmt.Printf("Error: %v", err.Error())
			}
		} else {
			if configFile == nil {
				die("No config")
			}
			for _, a := range configFile.Accounts {
				if a.Name == *account {
					if err := DoGenerate(a.Secret); err != nil {
						fmt.Printf("Error: %v", err.Error())
					}
					os.Exit(0)
				}
			}
			die("No account found")
		}

	case passphrase.FullCommand():
		if configFile == nil {
			die("No config")
		}
		var passphrase, err = GetNewPassword()
		if err != nil {
			die(err.Error())
		}
		if passphrase != nil {
			iv := make([]byte, aes.BlockSize)
			if _, err := io.ReadFull(rand.Reader, iv); err != nil {
				die(err.Error())
			}
			configFile.EncryptionConfig = &EncryptionConfig{
				Key: passphrase,
				IV:  iv,
			}
		} else {
			configFile.EncryptionConfig = nil
		}
		SaveConfig(configFile)

	case list.FullCommand():
		if configFile == nil {
			fmt.Println("No config")
		} else {
			for _, account := range configFile.Accounts {
				fmt.Println(account.Name)
			}
		}
	}
}
